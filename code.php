<?php

$input = simplexml_load_file("test1.xml");
$output = simplexml_load_file("steckbriefe.xml");

$studentNumber = 2;

$content = array();
$students = array();
$fields = array();

$input->rewind();

for($j = 0; $j < $studentNumber; $j++){
    $i = 0;
    foreach ($input->getChildren() as $student) {
      $students[$j][$i] = $student;
      $i++;
    }
    $input->next();
}

for($i = 0; $i < count($students); $i++){
    for($j = 0; $j < count($students[$i]); $j++){
        $students[$i][$j]->rewind();
        $students[$i][$j]->next();
        $cache = $students[$i][$j]->current();
        $content[$i][$j] = $cache->__toString();
    }
}

$frage = array();
$antwort = array();
$questionFirst = array();
$ratings = array();
$ratingsNames = array("witze", "parken", "lügen", "kochen", "nervig");

for($i = 0; $i < $studentNumber; $i++){
    $ratingsCounter = 0;
    for($j = 0; $j < count($content[$i]); $j++){
        //store the current content for easier access
        $cache = $content[$i][$j];
        $positionFrage = strpos($cache, "XY:");
        $positionAntwort = strpos($cache, "MM:");

        if (is_numeric($cache)){
            $ratings[$i][$ratingsCounter] = $cache;
            $ratingsCounter++;
            continue;
        }

        if($positionFrage < 0 && $positionAntwort < 0){
            echo "negative Number for positions found in run " + $i + ". " + $j;
            continue;
        }

        if($positionFrage < $positionAntwort){
            $questionFirst[$i][$j] = true;
            $frage[$i][$j] = trim(str_replace("XY:", "", substr($cache, 0, $positionAntwort -1)));
            $antwort[$i][$j] = trim(str_replace("MM:", "", substr($cache, $positionAntwort)));
        } else if($positionFrage > $positionAntwort) {
            $questionFirst[$i][$j] = false;
            $antwort[$i][$j] = trim(str_replace("MM:", "", substr($cache, 0, $positionFrage -1)));
            $frage[$i][$j] = trim(str_replace("XY:", "", substr($cache, $positionFrage)));
        }
    }
}


for($i = 0; $i < $studentNumber; $i++){
    $currentSteckbrief = $output->steckbrief[$i];

    for($j = 0; $j < 8; $j++) {
        if($questionFirst[$i][$j] == false){
            $currentSteckbrief->addChild("frage", $frage[$i][$j]);
            $currentSteckbrief->addChild("antwort", $antwort[$i][$j]);
        } else if($questionFirst[$i][$j] == true) {
            $currentSteckbrief->addChild("antwort", $antwort[$i][$j]);
            $currentSteckbrief->addChild("frage", $frage[$i][$j]);
        }
    }
    for($j = 0; $j < count($ratingsNames); $j++){
        $currentSteckbrief->addAttribute($ratingsNames[$j], $ratings[$i][$j]);
    }
}

$output->asXML("output.xml");
?>